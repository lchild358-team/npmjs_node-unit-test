module.exports = exports = (function(){

  const color = require('colors/safe');

  /**
   * Class AssertError
   * @param message string Assert message
   * @param expected mixed Expected value
   * @param actual mixed Actual value
   */
  var AssertError = function(message, expected, actual){

    this.message  = message;
    this.expected = expected;
    this.actual   = actual;
  };

  /** */
  AssertError.prototype = new Error();

  /**
   * @param obj mixed Source object
   * @return string
   */
  AssertError._objToDspString = function(obj){

    if(typeof obj === 'undefined'){
      return 'UNDEFINED';
    }else if(typeof obj === 'object'){
      if(obj === null){
        return 'NULL';
      }else{
        return JSON.stringify(obj, null, 4);
      }
    }else if(typeof obj === 'string'){
      if(obj === ''){
        return 'EMPTY STRING';
      }else{
        return `'${obj}'`;
      } 
    }else if(typeof(obj) === 'boolean'){
      return obj ? 'TRUE' : 'FALSE';
    }else if(typeof(obj) === 'function'){
      return obj();
    }else{
      return obj;
    }
  };

  /**
   *
   */
  AssertError.prototype.toString = function(){

    return `${color.yellow('Assert Error')}${color.gray(':')} ${this.message} ${color.gray('(expected:')} ${color.green(AssertError._objToDspString(this.expected))}${color.gray(', actual:')} ${color.yellow(AssertError._objToDspString(this.actual))}${color.gray(')')}`;
  };

  var AssertFailError = function(message){
    this.message = message;
  };

  /** */
  AssertFailError.prototype = new AssertError();

  /**
   *
   */
  AssertFailError.prototype.toString = function(){
    return `${color.yellow('Assert Error')}${color.gray(':')} ${this.message}`;
  };


  /**
   *
   */
  var Assert = function(){
    this.assertCnt = 0;
    this.errorCnt = 0;

    this.assert = function(expected, actual, message){
      this.assertCnt++;
      this.errorCnt++;
      throw new AssertError(message, expected, actual);
    };

    this.fail = function(message){
      this.assertCnt++;
      this.errorCnt++;
      throw new AssertFailError(message);
    }

    this.assertEquals = function(expected, actual, message){
      this.assertCnt++;
      if(expected !== actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertEquals failed', expected, actual);
      }
      return this;
    };

    this.assertNotEquals = function(expected, actual, message){
      this.assertCnt++;
      if(expected === actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotEquals failed', ()=>{return `NOT ${AssertError._objToDspString(expected)}`;}, actual);
      }
      return this;
    };

    this.assertSame = function(expected, actual, message){
      this.assertCnt++;
      if(expected != actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertSame failed', expected, actual);
      }
      return this;
    };

    this.assertNotSame = function(expected, actual, message){
      this.assertCnt++;
      if(expected == actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotSame failed', ()=>{return `NOT ${AssertError._objToDspString(expected)}`;}, actual);
      }
      return this;
    };

    this.assertUndefined = function(actual, message){
      this.assertCnt++;
      if(typeof actual !== 'undefined'){
        this.errorCnt++;
        throw new AssertError(message ? message: 'assertUndefined failed', undefined, actual);
      }
      return this;
    };

    this.assertNotUndefined = function(actual, message){
      this.assertCnt++;
      if(typeof actual === 'undefined'){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotUndefined failed', ()=>{return 'NOT UNDEFINED';}, actual);
      }
      return this;
    };

    this.assertNull = function(actual, message){
      this.assertCnt++;
      if(typeof(actual) !== 'object' || actual !== null){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNull failed', null, actual);
      }
      return this;
    };

    this.assertNotNull = function(actual, message){
      this.assertCnt++;
      if(typeof(actual) === 'object' && actual === null){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotNull failed', ()=>{return 'NOT NULL';}, actual);
      }
      return this;
    };

    this.assertTrue = function(actual, message){
      this.assertCnt++;
      if(typeof(actual) !== 'boolean' || ! actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertTrue failed', true, actual);
      }
      return this;
    };

    this.assertFalse = function(actual, message){
      this.assertCnt++;
      if(typeof(actual) !== 'boolean' || actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertFalse failed', false, actual);
      }
      return this;
    };

    this.assertTypeof = function(expected, actual, message){
      this.assertCnt++;
      if(typeof(actual) != expected){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertTypeof failed', expected, typeof(actual));
      }
      return this;
    };

    this.assertNotTypeof = function(expected, actual, message){
      this.assertCnt++;
      if(typeof(actual) == expected){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotTypeof failed', ()=>{return `NOT '${expected}'`;}, typeof(actual));
      }
      return this;
    };

    this.assertInstanceof = function(expected, actual, message){
      this.assertCnt++;
      if(! (actual instanceof expected)){
        this.errorCnt++;
        var match1 = expected.toString().match(/^function (.*)\(/);
        var match2 = Object.prototype.toString.call(actual).match(/^\[object (.*)\]/);
        throw new AssertError(message ? message : 'assertInstance failed', match1 ? match1[1] : expected, match2 ? match2[1] : actual);
      }
      return this;
    };

    this.assertClassName = function(expected, actual, message){
      this.assertCnt++;
      var match = Object.prototype.toString.call(actual).match(/^\[object (.*)\]/);
      if(! match || match[1] !== expected){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertClass failed', expected, match ? match[1] : `primary type [${typeof(actual)}]`);
      }
      return this;
    };

    this.assertRequired = function(actual, message){
      this.assertCnt++;
      if(! actual){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertRequired failed', ()=>{return 'REQUIRED';}, actual);
      }
      return this;
    };

    this.assertEmpty = function(actual, message){
      this.assertCnt++;
      if(actual){
        this.errorCnt++;
        throw new AssertError(message ? message: 'assertEmpty failed', ()=>{return 'EMPTY';}, actual);
      }
      return this;
    };

    this.assertError = function(callback, message){
      this.assertCnt++;
      if(typeof callback !== 'function'){
        this.errorCnt++;
        throw new Error(`Invalid parameter for assertError, require function (actual: ${typeof callback})`);
      }

      var errflg = false;
      try{
        callback();
      }catch(error){
        // This is ok
        errflg = true;
      }finally{
        if(! errflg){
          this.errorCnt++;
          throw new AssertError(message ? message : 'assertError failed', ()=>{return 'ERROR';}, ()=>{return 'NOT ERROR';});
        }
      }

      return this;
    };

    this.assertNotError = function(callback, message){
      this.assertCnt++;
      if(typeof callback !== 'function'){
        this.errorCnt++;
        throw new Error(`Invalid parameter for assertNotError, require function (actual: ${typeof callback})`);
      }

      try{
        callback();
      }catch(error){
        this.errorCnt++;
        throw new AssertError(message ? message : 'assertNotError failed', ()=>{return 'NOT ERROR';}, ()=>{return `ERROR (${error.toString()})`;});
      }
      
      return this;
    };

    this.toString = function(){
      return `Assert: ${color.bold(this.assertCnt)}, Error: ${color.bold.red(this.errorCnt)}`;
    };

    this.cntAssert = function(){
      return this.assertCnt;
    };

    this.cntError = function(){
      return this.errorCnt;
    };

    this.hasError = function(){
      return this.errorCnt > 0;
    };
  };

  /**
   * @param arg function|object
   */
  var TestCase = function(arg){
    // Assertion
    this.assert = new Assert();
    // Errors
    this.assertErrors = {};
    this.assertErrorCnt = 0;
    this.errors = {};
    this.errorCnt = 0;
    this.testcaseCnt = 0;
    // Test
    if(typeof arg === 'function'){
      this.test = arg();
    }else if(typeof arg === 'object'){
      this.test = arg;
    }

    this.cntTestcase = function(){
      return this.testcaseCnt;
    };

    this.cntAssertError = function(){
      return this.assertErrorCnt;
    };

    this.cntError = function(){
      return this.errorCnt;
    };

    this.cntTotalError = function(){
      return this.errorCnt + this.assertErrorCnt;
    };

    this.hasError = function(){
      return this.errorCnt > 0 || this.assertErrorCnt > 0;
    };

    this.getAssertErrorsString = function(){
      var s = [];
      for(var name in this.assertErrors){
        s[s.length] = `${color.bold('\u25CF')} ${name}${color.gray(':')}\n    ${this.assertErrors[name].toString()}\n`;
      }
      return s.join('\n');
    };

    this.getErrorsString = function(){
      var s = [];
      for(var name in this.errors){
        s[s.length] = `${color.bold('\u25CF')} ${name}${color.gray(':')}\n    ${this.errors[name].toString().replace('Error:', color.red('Error:'))}\n`;
      }
      return s.join('\n');
    };

    this.getAssert = function(){
      return this.assert;
    };

    /**
     * Run test
     */
    this.run = function(){
      if(typeof(this.test) !== 'object' || this.test === null) return;

      for(var name in this.test){
        var match = /^test(.*)$/.exec(name); 
        if(match && typeof(this.test[name]) === 'function'){
          this.testcaseCnt++;
          var basename = match[1];

          // Find data provider and get data
          var data = [undefined];
          var dataProvName = `data${basename}`;
          if(typeof(this.test[dataProvName]) === 'function'){
            var tmp = this.test[dataProvName]();
            if(typeof(tmp) === 'object' && (tmp instanceof Array) && tmp.length > 0){
              data = tmp;
            }else{
              data = [tmp];
            }
          }

          var datacnt = data.length;

          for(var dataIdx in data){
            var dataItem = data[dataIdx];
            // Error flag
            var errflg = false;
            try{
              // Do test
              this.test[name].call(this.test, this.assert, dataItem);
            
            }catch(error){
              errflg = true;

              if(error instanceof AssertError){
                if(datacnt <= 1){
                  this.assertErrors[color.underline(name)] = error;
                }else{
                  this.assertErrors[`${color.underline(name)} ${color.gray('[data')} ${color.yellow('#')} ${color.yellow(dataIdx)}${color.gray(']')}`] = error;
                }
                this.assertErrorCnt++;
              }else{
                if(datacnt <= 1){
                  this.errors[color.underline(name)] = error;
                }else{
                  this.errors[`${color.underline(name)} ${color.gray('[data')} ${color.yellow('#')} ${color.yellow(dataIdx)}${color.gray(']')}`] = error;
                }
                this.errorCnt++;
              }
            }finally{
              if(datacnt <= 1){
                /* */ console.log(`${color.gray('[')} ${errflg ? color.red('\u2718') : color.green('\u2714')} ${color.gray('] Test')} ${name}`);
              }else{
                /* */ console.log(`${color.gray('[')} ${errflg ? color.red('\u2718') : color.green('\u2714')} ${color.gray('] Test')} ${name} ${color.gray('[data')} ${color.yellow('#')} ${color.yellow(dataIdx)}${color.gray(']')}`);
              }
            }
          }
        }
      }
    };
  };

  var TestSuite = function(){
    
    this.testcases = {};

    this.addFile = function(file){
      this.testcases[file] = new TestCase(require(file));
      return this;
    };

    this.execute = function(){
      /* */ console.log('');

      let error = false;
      for(var name in this.testcases){
        var testCase = this.testcases[name];
        /* */ console.log(color.cyan(`Run test '${name}'`));
        /* */ console.log(color.gray('=============================================='));
        testCase.run();
        if(testCase.hasError()){
          /* */ console.log(color.gray('----------------------------------------------'));
          if(testCase.cntAssertError() > 0){
            /* */ console.log(testCase.getAssertErrorsString());
          }
          if(testCase.cntError() > 0){
            /* */ console.log(testCase.getErrorsString());
          }
        }
        /* */ console.log(color.gray('=============================================='));
        /* */ console.log(`Result: ${testCase.hasError() ? color.bold.red('FAILED') : color.bold.green('PASSED')} (Testcase: ${color.bold(testCase.cntTestcase())}, Failed: ${color.yellow.bold(testCase.cntTotalError())}, ${testCase.getAssert().toString()})\n`);
        
        error |= testCase.hasError();
      }

      return ! error;
    };
  };

  return TestSuite;

})();

