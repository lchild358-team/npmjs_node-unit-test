# node-unit-test

Lightweight library for doing unit-test on NodeJs

### What's new

v1.3.1
* Fix bug: Could not run multiple testcases testing

v1.3.0
* Display result in color

v1.2.0
* Add data provider
* Fix bug of the execute method to return the test result as true/false value

v1.1.0
* Add assert method 'assertError'
* Add assert method 'assertNotError'

## Install

```bash
npm install node-unit-test
```

## Usage

### TestSuite
```javascript
var TestSuite = require('node-unit-test');
var testSuite = new TestSuite();

var result = testSuite
.addFile('/path/to/test/file/testcase_1.js')
.addFile('/path/to/test/file/testcase_2.js')
.execute();

process.exit(result ? 0 : 1);
```

### TestCase
Example of testcase file:
```javascript
module.exports = function(){

  return {
    test_something: (assert)=>{
      // Code here
    },
    test_another: (assert)=>{
      // Code here
    }
  };
};
```

### Data provider
Data provider function is the function that have same name with the test function, begin with the prefix 'data'.
```javascript
module.exports = function(){

  return {
    // This is data provider for the test_something
    data_something: ()=>{
      return [
        // mixed Data item 1,
        // mixed Data item 2,
        // ...
      ];
    },
    test_something: (assert, dataItem)=>{
      // Do something with given dataItem
    }
  };
};
```

### Assert
fail
```javascript
assert.fail(message)
```
assert
```javascript
assert.assert(expected, actual, message)
```
assertEquals
```javascript
assert.assertEquals(expected, actual, message)
```
assertNotEquals
```javascript
assert.assertNotEquals(expected, actual, message)
```
assertSame
```javascript
assert.assertSame(expected, actual, message)
```
assertNotSame
```javascript
assert.assertNotSame(expected, actual, message)
```
assertUndefined
```javascript
assert.assertUndefined(actual, message)
```
assertNotUndefined
```javascript
assert.assertNotUndefined(actual, message)
```
assertNull
```javascript
assert.assertNull(actual, message)
```
assertNotNull
```javascript
assert.assertNotNull(actual, message)
```
assertTrue
```javascript
assert.assertTrue(actual, message)
```
assertFalse
```javascript
assert.assertFalse(actual, message)
```
assertTypeof
```javascript
assert.assertTypeof(expected, actual, message)
```
assertNotTypeOf
```javascript
assert.assertNotTypeof(expected, actual, message)
```
assertRequired
```javascript
assert.assertRequired(actual, message)
```
assertEmpty
```javascript
assert.assertEmpty(actual, message)
```
assertInstanceof
```javascript
assert.assertInstanceof(expected, actual, message)
```
assertClassName
```javascript
assert.assertClassName(expected, actual, message)
```
assertError
```javascript
assert.assertError(callback, message)
```
assertNotError
```javascript
assert.assertNotError(callback, message)
```

### Result
```bash
Run test './test-node-unit-test.js'
==============================================
[ ✘ ] Test test_fail
[ ✘ ] Test test_assert
[ ✘ ] Test test_assert_equals
[ ✘ ] Test test_assert_not_equals
[ ✘ ] Test test_assert_same
[ ✘ ] Test test_assert_not_same
[ ✘ ] Test test_assert_undefined
[ ✘ ] Test test_assert_not_undefined
[ ✘ ] Test test_assert_null
[ ✘ ] Test test_assert_not_null
[ ✘ ] Test test_assert_true
[ ✘ ] Test test_assert_false
[ ✘ ] Test test_assert_typeof
[ ✘ ] Test test_assert_not_typeof
[ ✘ ] Test test_assert_required
[ ✘ ] Test test_assert_empty
[ ✘ ] Test test_assert_instanceof
[ ✘ ] Test test_assert_class_name
[ ✘ ] Test test_assert_error
[ ✘ ] Test test_assert_not_error
[ ✘ ] Test test_assert_data_provider_one
[ ✔ ] Test test_assert_data_provider_many [data # 0]
[ ✘ ] Test test_assert_data_provider_many [data # 1]
[ ✘ ] Test test_system_error_1
[ ✘ ] Test test_system_error_2 [data # 0]
[ ✘ ] Test test_system_error_2 [data # 1]
[ ✔ ] Test test_ok
----------------------------------------------
● test_fail:
    Assert Error: This is fail

● test_assert:
    Assert Error: This is error (expected: 'expected', actual: 'actual')

● test_assert_equals:
    Assert Error: Not equals (expected: 1, actual: '1')

● test_assert_not_equals:
    Assert Error: Equals (expected: NOT 1, actual: 1)

● test_assert_same:
    Assert Error: Not same (expected: 1, actual: 2)

● test_assert_not_same:
    Assert Error: Same (expected: NOT 1, actual: '1')

● test_assert_undefined:
    Assert Error: Not undefined (expected: UNDEFINED, actual: NULL)

● test_assert_not_undefined:
    Assert Error: Undefined (expected: NOT UNDEFINED, actual: UNDEFINED)

● test_assert_null:
    Assert Error: Not null (expected: NULL, actual: UNDEFINED)

● test_assert_not_null:
    Assert Error: Null (expected: NOT NULL, actual: NULL)

● test_assert_true:
    Assert Error: False (expected: TRUE, actual: FALSE)

● test_assert_false:
    Assert Error: True (expected: FALSE, actual: TRUE)

● test_assert_typeof:
    Assert Error: Not string (expected: 'string', actual: 'number')

● test_assert_not_typeof:
    Assert Error: String (expected: NOT 'string', actual: 'string')

● test_assert_required:
    Assert Error: Empty (expected: REQUIRED, actual: EMPTY STRING)

● test_assert_empty:
    Assert Error: Not empty (expected: EMPTY, actual: 123)

● test_assert_instanceof:
    Assert Error: Not array (expected: 'Array', actual: 'Number')

● test_assert_class_name:
    Assert Error: Object (expected: 'Object', actual: 'Array')

● test_assert_error:
    Assert Error: Not error (expected: ERROR, actual: NOT ERROR)

● test_assert_not_error:
    Assert Error: Error (expected: NOT ERROR, actual: ERROR (Error: This is error))

● test_assert_data_provider_one:
    Assert Error: Must be "Hello world" (expected: 'Hello world', actual: 'Hello')

● test_assert_data_provider_many [data # 1]:
    Assert Error: This is error (expected: '123', actual: '321')

● test_system_error_1:
    Error: This is system error 1

● test_system_error_2 [data # 0]:
    Error: This is system error 2

● test_system_error_2 [data # 1]:
    Error: This is system error 2

==============================================
Result: FAILED (Testcase: 25, Failed: 25, Assert: 41, Error: 22)
```
## License

[ISC](https://opensource.org/licenses/ISC)

