module.exports = function(){
  
  return {
    test_fail: (assert)=>{
      assert.fail('This is fail');
    },
    test_assert: (assert)=>{
      assert.assert('expected', 'actual', 'This is error');
    },
    test_assert_equals: (assert)=>{
      assert.assertEquals(1, 1, 'Equals');
      assert.assertEquals(1, '1', 'Not equals');
    },
    test_assert_not_equals: (assert)=>{
      assert.assertNotEquals(1, '1', 'Not equals');
      assert.assertNotEquals(1, 1, 'Equals');
    },
    test_assert_same: (assert)=>{
      assert.assertSame(1, '1', 'Same');
      assert.assertSame(1, 2, 'Not same');
    },
    test_assert_not_same: (assert)=>{
      assert.assertNotSame(1, 2, 'Not same');
      assert.assertNotSame(1, '1', 'Same');
    },
    test_assert_undefined: (assert)=>{
      assert.assertUndefined(undefined, 'Undefined');
      assert.assertUndefined(null, 'Not undefined');
    },
    test_assert_not_undefined: (assert)=>{
      assert.assertNotUndefined(null, 'Not undefined');
      assert.assertNotUndefined(undefined, 'Undefined');
    },
    test_assert_null: (assert)=>{
      assert.assertNull(null, 'Null');
      assert.assertNull(undefined, 'Not null');
    },
    test_assert_not_null: (assert)=>{
      assert.assertNotNull(undefined, 'Not null');
      assert.assertNotNull(null, 'Null');
    },
    test_assert_true: (assert)=>{
      assert.assertTrue(true, 'True');
      assert.assertTrue(false, 'False');
    },
    test_assert_false: (assert)=>{
      assert.assertFalse(false, 'False');
      assert.assertFalse(true, 'True');
    },
    test_assert_typeof: (assert)=>{
      assert.assertTypeof('string', 'abc', 'String');
      assert.assertTypeof('string', 0.0, 'Not string');
    },
    test_assert_not_typeof: (assert)=>{
      assert.assertNotTypeof('string', 0, 'Not string');
      assert.assertNotTypeof('string', 'abc', 'String');
    },
    test_assert_required: (assert)=>{
      assert.assertRequired(123, 'Required');
      assert.assertRequired('', 'Empty');
    },
    test_assert_empty: (assert)=>{
      assert.assertEmpty('', 'Empty');
      assert.assertEmpty(123, 'Not empty');
    },
    test_assert_instanceof: (assert)=>{
      assert.assertInstanceof(Array, [], 'Array');
      assert.assertInstanceof(Array, 0.0, 'Not array');
    },
    test_assert_class_name: (assert)=>{
      assert.assertClassName('Object', new Array(), 'Object');
      assert.assertClassName('Array', new Date(), 'Not array');
    },
    test_assert_error: (assert)=>{
      assert.assertError(()=>{
        throw new Error('This is error');
      }, 'Error');
      assert.assertError(()=>{}, 'Not error');
    },
    test_assert_not_error: (assert)=>{
      assert.assertNotError(()=>{}, 'Not error');
      assert.assertNotError(()=>{
        throw new Error('This is error');
      }, 'Error');
    },
    data_assert_data_provider_one: ()=>{
      return 'Hello world';
    },
    test_assert_data_provider_one: (assert, data)=>{
      assert.assertEquals(data, 'Hello', 'Must be "Hello world"');
    },
    data_assert_data_provider_many: ()=>{
      return [
        {
          expected: '123',
          actual: '123',
          message: 'This is ok'
        },
        {
          expected: '123',
          actual: '321',
          message: 'This is error'
        }
      ];
    },
    test_assert_data_provider_many: (assert, data)=>{
      assert.assertEquals(data.expected, data.actual, data.message);
    },
    test_system_error_1: (assert)=>{
      throw new Error('This is system error 1');
    },
    data_system_error_2: ()=>{
      return [1,2];
    },
    test_system_error_2: (assert, value)=>{
      throw new Error('This is system error 2');
    },
    test_ok: (assert)=>{
      assert.assertTrue(true);
    }
  }
};
